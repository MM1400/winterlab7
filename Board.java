public class Board {
	private Square[][] tictactoeBoard;
	public Board() {
		this.tictactoeBoard = new Square[3][3];
		for(int i =0; i < tictactoeBoard.length; i++) {
			for(int j =0; j < tictactoeBoard[i].length; j++) {
				this.tictactoeBoard[i][j] = Square.BLANK;
			}
		}
	}
	public String toString() {
		String s = "";
		for(int i = 0; i < this.tictactoeBoard.length; i++) {
			for(int j= 0; j< this.tictactoeBoard[i].length; j++) {
				s+=(this.tictactoeBoard[i][j]);
			}
			s+= "\n";
		}
		return s;
	}
	public boolean placeToken(int row, int col, Square playerToken) {
		if(row > 3 || col > 3 || row < 1 || col < 1) {
			return false;
		}
		if(this.tictactoeBoard[row-1][col-1] == Square.BLANK) {
			this.tictactoeBoard[row-1][col-1] = playerToken;
			return true;
		} else {
			return false;
		}
	}
	public boolean checkIfFull() {
		for(int i =0; i < this.tictactoeBoard.length; i++) {
			for(int j =0; j < this.tictactoeBoard[i].length; j++) {
				if(this.tictactoeBoard[i][j] == Square.BLANK) {
					return false;
				}
			}
		}
		return true;
	}
	private boolean checkIfWinningHorizontal(Square playerToken) {
		int rowCount = 0;
		for(int i = 0; i < this.tictactoeBoard.length; i++) {
			for(int j = 0; j < this.tictactoeBoard[i].length; j++) {
				if(this.tictactoeBoard[i][j] == playerToken) {
				rowCount++;
				}
			}
		if(rowCount == 3) {
		return true;
		}
			rowCount = 0;
		}
			return false;
	}
	private boolean checkIfWinningVertical(Square playerToken) {
		int colCount = 0;
		for(int i = 0; i < this.tictactoeBoard.length; i++) {
			for(int j = 0; j < this.tictactoeBoard[i].length; j++) {
				if(this.tictactoeBoard[j][i] == playerToken) {
					colCount++;
				}
			}
			if(colCount == 3) {
				return true;
			}
			colCount = 0;
		}
		return false;
	}
	public boolean checkIfWinning(Square playerToken) {
		if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken)) {
			return true;
		}else {
			return false;
		}
	}
}