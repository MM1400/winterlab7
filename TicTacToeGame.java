import java.util.Scanner;
public class TicTacToeGame {
	public static void main(String[] args) {
		System.out.println("Welcome to tic tac toe!");
		System.out.println("Player 1 is : X and Player 2 is : O");
		Board board = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		while(!gameOver) {
			System.out.println(board);
			if(player == 1) {
				playerToken = Square.X;
			}else {
				playerToken = Square.O;
			}
			System.out.println("Player " + player + ", it's your turn");
			Scanner scan = new Scanner(System.in);
			System.out.println("Enter the row");
			int row = scan.nextInt();
			System.out.println("Enter the column");
			int col = scan.nextInt();
			
			while(!board.placeToken(row, col, playerToken)) {
			System.out.println("Enter a valid row");
			row = scan.nextInt();
			System.out.println("Enter a valid column");
			col = scan.nextInt();
			}
			if(board.checkIfFull()) {
				System.out.println("Its a tie");
				gameOver = true;
			}else if (board.checkIfWinning(playerToken)) {
				System.out.println("Player " + player + " is the winner");
				gameOver = true;
			} else {
				player++;
				if(player > 2 ) {
						player =  1;
				}
			}
		}
		System.out.println(board);
	}
}